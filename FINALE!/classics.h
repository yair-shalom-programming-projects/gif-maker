/*********************************
* Class: MAGSHIMIM Final Project *
* Play function declaration	 	 *
**********************************/

#ifndef STANDARTH
#define STANDARTH

#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#define TRUE 1
#define FALSE 0

#define STR_LEN 50

void getNum(int* num);
char* getStr(void);

#endif