#ifndef LINKEDLISTH
#define LINKEDLISTH



#include <errno.h>
#include "unistd.h"
#include "classics.h"


// Frame struct
typedef struct Frame
{
	char* name;
	unsigned int duration;
	char* path;
} Frame;


// Link (node) struct
typedef struct FrameNode
{
	Frame* frame;
	struct FrameNode* next;
} FrameNode;



FrameNode* findName(FrameNode* head, char* name);
void getNewFrame(FrameNode* head);
int handleInvalidInput(FrameNode* head, char* path, int* duration, char** name);
void removeFrame(FrameNode* head);
void changeFrameIndex(FrameNode* head);
void changeFrameDuration(FrameNode* head);
void changeAllFramesDuration(FrameNode* head);
void printFrameList(FrameNode* head);
void saveProject(FrameNode* head);

void freeList(FrameNode* cur);


#define MAGSHIMAKER_SIGN "MagshiMakerFile"
#define MAGSHIMAKER_SIGN_FORMAT "%s\n\n"
#define FRAME_FILE_FORMAT "%s -> %d ms -> %s\n"
#define FILE_TYPE ".txt"


#endif