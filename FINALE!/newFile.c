#include "newFile.h"


/*
create new project
input: n/a
output: project
*/
FrameNode* createFile(void)
{
	FrameNode* list = NULL;
	

	list = (FrameNode*)calloc(1, sizeof(FrameNode));
	list->frame = (Frame*)calloc(1, sizeof(Frame));
	list->next = NULL;

	return list;

}
/*
load an existing project
input: n/a
output: head pointer to the project
*/
FrameNode* loadFile(void)
{

	FILE* file = NULL;

	FrameNode* list = createFile();
	FrameNode* cur = NULL;
	
	char* filePath = NULL;
	char* fileSignature = (char*)calloc(SIGNATURE_LEN + 1, sizeof(char));



	printf("Enter the path of the project (including project name):\n");
	filePath = getStr();

	//open file
	file = fopen(filePath, "r");


	//check if path can be access, try copy len(sinature) + 1 (null) bytes to check if signature can even fit in file, if the copy completed, check if magshimaker signature is in file and if FILE_TYPE is the file type
	if ( !access(filePath, F_OK) && fscanf(file, MAGSHIMAKER_SIGN_FORMAT, fileSignature) && !strcmp(fileSignature, MAGSHIMAKER_SIGN) && !strcmp(strrchr(filePath, '\0') - strlen(FILE_TYPE), FILE_TYPE))
	{

		cur = list;

		//read on file until End of file
		while (!feof(file))
		{

			//free space for future values
			cur->next = (FrameNode*)calloc(1, sizeof(FrameNode));
			cur->next->frame = (Frame*)calloc(1, sizeof(Frame));

			cur->next->frame->name = (char*)calloc(STR_LEN, sizeof(char));
			cur->next->frame->path = (char*)calloc(STR_LEN, sizeof(char));

			//try scanning values from file. if faill - file is bad
			fscanf(file, FRAME_FILE_FORMAT, cur->next->frame->name, &cur->next->frame->duration, cur->next->frame->path);
			
			cur = cur->next;
		
		}
	}	
	else
	{

		printf("Error!- cant open file, creating a new project\n");
	}

	free(fileSignature);
	fileSignature = NULL;
	free(filePath);
	filePath = NULL;

	//if file exist - close it
	if (file)
	{
		fclose(file);
		file = NULL;
	}

	return list;

}