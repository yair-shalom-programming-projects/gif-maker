#include "classics.h"

/*
Get a number from the user.
input: pointer to number
output: na/
*/
void getNum(int* num)
{
	scanf_s("%d", num);
	getchar();
}
/*
get string from the user.
input: n/a
output: string
*/
char* getStr(void)
{
	// add 1 more byte space for NULL
	char* str = (char*)calloc(STR_LEN + 1, sizeof(char));
	fgets(str, STR_LEN, stdin);

	//delete '\n' at the end (added by 'fgets' func) 
	str[strlen(str) - 1] = 0;
	//reallocate space to the length of the string
	str = (char*)realloc(str, strlen(str) + 1);

	return str;
}