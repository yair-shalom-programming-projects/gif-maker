
#include "linkedList.h"


/*
Get new frame from the user and adds it to list of frames.
input: head frames list pointer
ouput: n/a
*/
void getNewFrame(FrameNode* head)
{
	FrameNode* newFrame = head;
	
	int duration = 0;
	char* path = NULL;
	char* name = NULL;


	//Move to last frame
	while (newFrame->next)
	{
		newFrame = newFrame->next;
	}


	printf("*** Creating new frame ***\n");
	
	//get frame data
	printf("Please insert frame path:\n");
	path = getStr();
	printf("Please insert frame duration(in miliseconds):\n");
	getNum(&duration);
	printf("Please choose a name for that frame:\n");
	name = getStr();


	//check for invalid input - if so, get new input (or in case of wrong path, return BAD PATH)
	if (!handleInvalidInput(head, path, &duration, &name))
	{

		//---- Free memory for next frame ----
		newFrame->next = (FrameNode*)calloc(1, sizeof(FrameNode));
		newFrame->next->frame = (Frame*)calloc(1, sizeof(Frame));

		//---- Insert given data ----
		newFrame->next->frame->path = path;
		newFrame->next->frame->name = name;
		newFrame->next->frame->duration = duration;
	}
	else
	{
		free(path);
		free(name);
	}

}
/*
find given name in list of the frames
input: head pointer, name
output: name's place
*/
FrameNode* findName(FrameNode* head, char* name)
{
	FrameNode* cur = head;

	int nameFound = FALSE;
	int a = 0;

	//search in list for the given name and given index untill both found or until all frames have been checked
	while (!nameFound && cur->next)
	{

		//compare current frame's name to given name
		if (!strcmp(cur->next->frame->name, name))
		{
			nameFound = TRUE;
		}
		else
		{
			cur = cur->next;
		}
	}
	if (!nameFound)
	{
		cur = NULL;
	}
	return cur;

}
/*
get duration from the user
input: duration
output: n/a
*/
void getDuration(int* duration)
{
	while (*duration <= 0)
	{
		printf("The duration can't be negative, please enter diration again\n\n");
		getNum(duration);

	}
}
/*
handle all the validation to new frame's input - path, duration, name.. If input invalid, ask for new.
incase of wrong path input, return WRONGPATH
input: head pointer to list (not including first empty cell), path - input path, duration - input num, 
name - pointer to string (so if neccessery, value will be change)
output: good or bad path (TRUE OR FALSE)
*/
int handleInvalidInput(FrameNode* head, char* path, int* duration, char** name)
{

	int badPath = FALSE;


	//check if path exist - will enter if not
	if (access(path, F_OK))
	{

		printf("Can't find file! Frame will not be added\n\n");
		badPath = TRUE;
	}
	else
	{
		//while duration is negative, and path is good, ask for new duration
		getDuration(duration);

	
		//check if the name is valid (not taken), if not, and the rest input is valid, ask for new name
		while (findName(head, *name))
		{
			
			printf("The name is already taken, please enter another name\n");
			free(*name);
			*name = getStr();
		}
	}

	return badPath;

}
/*
remove a frame from the list of frames.
input: head pointer to list
output: n/a
*/
void removeFrame(FrameNode* head)
{
	FrameNode* namePlace = NULL;
	FrameNode* helper = NULL;
	
	char* name = NULL;

	
	printf("Enter the name of the frame you wish to erase\n");
	name = getStr();


	namePlace = findName(head, name);

	if (namePlace)
	{
		
		//save wanted to be delete frame to free his dynamic memory 
		helper = namePlace->next;
		//connect the pointer that pointed to the wanted to be delete frame to the WDF next frame
		namePlace->next = helper->next;

		//free all the dynamic memory in frame and reset to 0 
		free(helper->frame->name); // free name
		helper->frame->name = NULL;
		free(helper->frame->path); // free path
		helper->frame->path = NULL; 
		free(helper->frame); // free frame
		helper->frame = NULL;
		free(helper); // free node
		helper = NULL;
	}
	else
	{

		printf("Name not found :( \n");
	}

	free(name);

}
/*
change frame index in the list.
input: head pointer to list
output: n/a
*/
void changeFrameIndex(FrameNode* head)
{
	FrameNode* cur = head;
	FrameNode* namePointer = NULL;
	FrameNode* indexPointer = NULL;
	FrameNode* helper = NULL;

	char* name = NULL;
	int index = 0, counter = 1;


	//get name of frame and new index to place it
	printf("Enter the name of the frame\n");
	name = getStr();
	printf("Enter the new index in the movie you wish to place the frame\n");
	getNum(&index);
	

	//first valid index is 1
	if (index > 0)
	{

		//search in list for the given name and given index untill both found or until all frames have been checked
		while (cur->next && (!indexPointer || !namePointer))
		{

			//compare given name to current frame's name - 0 for equal.
			if (!strcmp(cur->next->frame->name, name))
			{

				//save the frame pointing to the wanted frame
				namePointer = cur;
			}
			else if (index == counter)
			{

				//if index found after frame (frame will be move foward), index pointer will point to the given index. else, index pointer will point to the frame that point at the given frame  
				if (namePointer)
				{

					indexPointer = cur->next;
				}
				else
				{

					indexPointer = cur;
				}
			}

			cur = cur->next;
			counter++;
		}

		//in other words, if index pointer and given frame found
		if (indexPointer && namePointer)
		{

			//get frame out of list
			helper = namePointer->next;
			namePointer->next = helper->next;

			//connect frame in index place
			helper->next = indexPointer->next;
			indexPointer->next = helper;
		}
		else if (!namePointer)
		{

			printf("Name not found :( \n");
		}
	}

	free(name);

}
/*
Change frame duration.
input: head
output: n/a
*/
void changeFrameDuration(FrameNode* head)
{
	FrameNode* namePlace = NULL;
	
	char* name = NULL;
	int newDuration = 0;


	//get info
	printf("Enter the name of the frame\n");
	name = getStr();
	printf("Enter new duration\n");
	getNum(&newDuration);



	namePlace = findName(head, name);
	
	if(namePlace)
	{
		getDuration(&newDuration);
		//change frame duration to the new given duration
		namePlace->next->frame->duration = newDuration;
		

	}
	else
	{

		printf("Name not found :(\n");
	}

	

	free(name);

}
/*
change all frames duration.
input: head pointer to frames list
output: n/a
*/
void changeAllFramesDuration(FrameNode* head)
{
	FrameNode* cur = head;
	int newDuration = 0;

	
	printf("Enter the duration for all frames:\n");
	getNum(&newDuration);
	//get if wrong input
	getDuration(&newDuration);

	
	//run on all list and change duration to the given new duration
	while (cur->next)
	{

		cur->next->frame->duration = newDuration;
		cur = cur->next;
	}

}
/*
print frame list.
input: head pointer to list
output: n/a
*/
void printFrameList(FrameNode* head)
{
	FrameNode* cur = head;
	

	printf("		Name	Duration	Path\n");
	
	//run on all list and print frame's name, frame's duration and frame's path
	while (cur->next)
	{

		printf("		%s	%d		%s\n", cur->next->frame->name, cur->next->frame->duration, cur->next->frame->path);
		cur = cur->next;
	}

}
/*
save project in the given path (if valid)
input: head pointer to list of frames
output: n/a
*/
void saveProject(FrameNode* head)
{
	FrameNode* cur = head;
	FILE* file = NULL;

	char* filePath = NULL;


	printf("Where to save the project? enter a full path and file name without file type\n");
	filePath = getStr();


	//realloc file to his length + null + length of the file type 
	filePath = (char*)realloc(filePath, strlen(filePath) + 1 + strlen(FILE_TYPE));
	
	//copy the file type to the filePath
	strcat(filePath, FILE_TYPE);


	//open file path and see if its valid	
	if (file = fopen(filePath, "w"))
	{

		//write magshim maker signature in file for future recognazie
		fprintf(file, MAGSHIMAKER_SIGN_FORMAT, MAGSHIMAKER_SIGN);

		//write frames info (name -> duration -> path\n) in file
		while (cur->next)
		{
			fprintf(file, FRAME_FILE_FORMAT, cur->next->frame->name, cur->next->frame->duration, cur->next->frame->path);
			cur = cur->next;
		}
		
		//close file
		fclose(file);
		file = NULL;
	}
	else
	{

		printf("Project didn't save. Path not exist\n");
	}

	//free string memory
	free( filePath );
	filePath = NULL;


}
/*
free all dynamic allocated memory in list - recursivley!
input: list pointer
output: n/a
*/
void freeList(FrameNode* cur)
{
	
	if (cur)
	{

		freeList(cur->next);

		//free current dynamic memory frame
		free(cur->frame->name);
		cur->frame->name = NULL;
		free(cur->frame->path);
		cur->frame->path = NULL;
		free(cur->frame);
		cur->frame = NULL;
		free(cur);
		cur = NULL;
	}

}