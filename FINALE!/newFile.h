#ifndef NEWFILEH
#define NEWFILEH


#include "classics.h"
#include "unistd.h"
#include "linkedList.h"


#define SIGNATURE_LEN strlen(MAGSHIMAKER_SIGN) + 1 // for NULL


FrameNode* createFile(void);
FrameNode* loadFile(void);

#endif