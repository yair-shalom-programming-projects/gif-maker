#ifndef MAINH
#define MAINH


#include "linkedList.h"
#include "classics.h"
#include "view.h"
#include "newFile.h"


#define BAD_INPUT -1

#define EXIT 0
#define ADD_NEW_FRAME 1
#define REMOVE_FRAME 2
#define CHANGE_FRAME_INDEX 3
#define CHANGE_FRAME_DURATION 4
#define CHANGE_FRAMES_DURATION 5
#define PRINT_LIST 6
#define PLAY_MOVIE 7
#define PLAY_MOVIE_REVERSED 8
#define SAVE_PROJECT 9

#define CREATE_FILE 0
#define LOAD_FILE 1


void printMenu(void);
void printOpenMsg(void);


#endif