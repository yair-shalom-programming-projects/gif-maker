/*********************************
* Class: MAGSHIMIM Final Project *
* Play function				 	 *
**********************************/

#include "view.h"

/**
play the movie!!
display the images each for the duration of the frame one by one and close the window
input: a linked list of frames to display
output: none
**/
void play(FrameNode* list)
{
	list = list->next;
	cvNamedWindow("Display window", CV_WINDOW_AUTOSIZE); //create a window
	FrameNode* head = list;
	int imgNum = 1, playCount = 0;
	IplImage* image;

	while (playCount < GIF_REPEAT)
	{
		while (list != 0)
		{
			image = cvLoadImage(list->frame->path, 1);
			IplImage* pGrayImg = 0;
			pGrayImg = cvCreateImage(cvSize(image->width, image->height), image->depth, 1);
			if (!image) //The image is empty - shouldn't happen since we checked already.
			{
				printf("Could not open or find image number %d", imgNum);
			}
			else
			{
				cvShowImage("Display window", image); //display the image
				cvWaitKey(list->frame->duration); //wait
				list = list->next;
				cvReleaseImage(&image);
			}
			imgNum++;
		}
		list = head; // rewind
		playCount++;
	}
	cvDestroyWindow("Display window");
}
/*
play a one cycle of the gif reversed RECURSIVLEY
input: a linked list of frames to display
output: none
*/
void playReversedCycle(FrameNode* cur)
{

	if (cur)
	{

		playReversedCycle(cur->next);

		cvNamedWindow("Display window", CV_WINDOW_AUTOSIZE); //create a window
		IplImage* image;


		image = cvLoadImage(cur->frame->path, 1);
		IplImage* pGrayImg = 0;
		pGrayImg = cvCreateImage(cvSize(image->width, image->height), image->depth, 1);
		cvShowImage("Display window", image); //display the image
		cvWaitKey(cur->frame->duration); //wait
		cur = cur->next;
		cvReleaseImage(&image);
	}
}
/*
play the movie reversed!!
display the images each for the duration of the frame one by one and close the window
input: a linked list of frames to display
output: none
*/
void playReversed(FrameNode* list)
{

	list = list->next;
	
	int playCount = 0;



	while (playCount < GIF_REPEAT)
	{

		playReversedCycle(list);
		playCount++;
	}
	
	cvDestroyWindow("Display window");

}