/*********************************
* Class: MAGSHIMIM C2			 *
* Week:                			 *
* Name:                          *
* Credits:                       *
**********************************/


#include "main.h"

int main(void)
{
	FrameNode* framesList = NULL;

	//add play to commandList 
	void(*commands_arr[])(FrameNode*) = { freeList, getNewFrame, removeFrame, changeFrameIndex, changeFrameDuration, changeAllFramesDuration, printFrameList, play, playReversed, saveProject };
	FrameNode*(*newFile[])() = { createFile, loadFile };

	int command = 0;
	int wrongCommand = 0;



	do
	{
		printOpenMsg();
		getNum(&command);

		if (command >= 0 && command < sizeof(newFile) / sizeof(*newFile))
		{
			framesList = newFile[command]();
			wrongCommand = FALSE;
		}
		else
		{
			printf("Wrong command\n\n");
			wrongCommand = TRUE;
		}
	} while (wrongCommand);


	do
	{

		printMenu();
		getNum(&command);

		//check for valid choice
		if (command >= 0 && command < sizeof(commands_arr) / sizeof(*commands_arr))
		{
			//call command
			commands_arr[command](framesList);
		
		}
		else
		{
			printf("Wrong command\n");
		}

	} while (EXIT != command);

	
	printf("Bye!\n");


	getchar();
	return 0;

}
/*
print opening message
input: n/a
output: n/a
*/
void printOpenMsg(void)
{

	printf("Welcome to Magshimim Movie Maker! what would you like to do?\n");
	printf(" [%d] Create a new project\n", CREATE_FILE);
	printf(" [%d] Load existing poject\n", LOAD_FILE);

}
/*
print the manu message to the user.
input: n/a
output: n/a
*/
void printMenu(void)
{

	printf("\nWelcome to Magshimim Movie Maker! what would you like to do?\n");
	printf(" [%d] Exit\n", EXIT);
	printf(" [%d] Add new Frame\n", ADD_NEW_FRAME);
	printf(" [%d] Remove a Frame\n", REMOVE_FRAME);
	printf(" [%d] Change frame index\n", CHANGE_FRAME_INDEX);
	printf(" [%d] Change frame duration\n", CHANGE_FRAME_DURATION);
	printf(" [%d] Change duration of all frames\n", CHANGE_FRAMES_DURATION);
	printf(" [%d] List frames\n", PRINT_LIST);
	printf(" [%d] Play movie!\n", PLAY_MOVIE);
	printf(" [%d] Play movie REVERSED!\n", PLAY_MOVIE_REVERSED);
	printf(" [%d] save project\n", SAVE_PROJECT);

}